package net.IceRhal.SnowballDmg;

import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class SnowballDmg extends JavaPlugin implements Listener {
	
	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@EventHandler
	public void EntityDamagebyEntity(EntityDamageByEntityEvent e) {
		
		if(e.getDamager() instanceof Snowball && e.getEntity() instanceof Player) {
			
			e.setDamage(2.0);
		}
	}

}
